package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/bogem/id3v2/v2"
)

var library_folder string

func after(str, substr string) string {
	pos := strings.LastIndexAny(str, substr)
	if pos == -1 {
		return str
	}
	adjustedPos := pos + len(substr)
	if adjustedPos >= len(str) {
		return ""
	}
	return str[adjustedPos:]
}

func does_exist(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}

func is_dir(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}

	if fileInfo.IsDir() {
		return true
	} else {
		return false
	}
}

func get_library_folder() (string, bool) {
	library_folder := os.Getenv("MUSICDIR")
	if library_folder == "" {
		if does_exist("~/music") && is_dir("~/music") {
			return "~/music/", true
		} else if does_exist("~/Music") && is_dir("~/Music") {
			return "~/Music/", true
		}
	}
	if library_folder[len(library_folder)-1:] != "/" {
		return library_folder + "/", true
	}

	return library_folder, true
}

func move_music_file(path_to_song string, tag *id3v2.Tag) bool {
	var err error
	var new_file_name string

	file_name := after(path_to_song, "/")

	if strings.Contains(tag.Artist(), "/") || strings.Contains(tag.Album(), "/") || strings.Contains(tag.Title(), "/") {
		new_file_name = file_name
	} else {
		new_file_name = tag.Artist() + " - " + tag.Title() + ".mp3"
	}

	fmt.Println("Current file: ", file_name)

	if tag.Artist() == "" && tag.Album() == "" {
		fmt.Fprintln(os.Stderr, "File doesn't have an artist or an album, file might not be an MP3 file, skipping.")
	}

	artist_folder := library_folder + tag.Artist() + "/"
	album_folder := artist_folder + tag.Album() + "/"

	if !does_exist(artist_folder) {
		fmt.Println("Creating a folder for the artist: ", tag.Artist())
		err = os.Mkdir(artist_folder, os.ModePerm)
		if err != nil {
			fmt.Fprintln(os.Stderr, "cli-music-organiser: failed to create artist folder")
			return false
		}
	}
	if !does_exist(album_folder) {
		fmt.Println("Creating a folder for the album: ", tag.Album())
		err = os.Mkdir(album_folder, os.ModePerm)
		if err != nil {
			fmt.Fprintln(os.Stderr, "cli-music-organiser: ", err)
			return false
		}
	}

	fmt.Println("Moving", file_name, " to", album_folder+new_file_name)
	err = os.Rename(path_to_song, album_folder+new_file_name)
	if err != nil {
		fmt.Fprintln(os.Stderr, "cli-music-organiser: ", err)
		return false
	}
	return true
}

func main() {
	if len(os.Args) == 1 {
		fmt.Fprintln(os.Stderr, "Not enough args passed to program")
		os.Exit(-1)
	}

	ok := false

	library_folder, ok = get_library_folder()
	if !ok {
		fmt.Fprintln(os.Stderr, "Failed to get library folder")
		os.Exit(-1)
	}

	for _, path := range os.Args[1:] {
		// TODO: Make sure that the path is a file
		// Might need to change the loop to be a function that is called?
		if is_dir(path) {
			fmt.Fprintln(os.Stderr, "Was passed a directory, please pass files, you can use a wildcard.")
			os.Exit(-1)
		}

		tag, err := id3v2.Open(path, id3v2.Options{Parse: true})
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error opening file: \"%s\", %v\n", path, err)
			continue
		}
		defer tag.Close()

		ok = move_music_file(path, tag)
		if !ok {
			os.Exit(-1)
		}

		fmt.Println()
	}

}
